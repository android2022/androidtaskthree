package by.dazerty.androidthree.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [ Contact::class ], version = 1, exportSchema = false)
abstract class ContactDatabase : RoomDatabase () {
    abstract fun contactDao() : ContactDao
}