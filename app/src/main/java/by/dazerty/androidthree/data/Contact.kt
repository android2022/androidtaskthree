package by.dazerty.androidthree.data

import androidx.annotation.NonNull
import androidx.room.Entity
import java.util.*

@Entity(primaryKeys = ["phone", "name", "lastname"])
data class Contact (
        val id : UUID = UUID.randomUUID(),
        @NonNull var phone : String = "",
        @NonNull var name : String = "",
        var lastname : String = "",
        var email: String = ""
) {
    override fun toString(): String {
        return "name - $name, surname - $lastname, phone - $phone, email - $email"
    }
}