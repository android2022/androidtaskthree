package by.dazerty.androidthree.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import java.util.*
import java.util.concurrent.Executors


private const val DATABASE_NAME = "contact-database"

class ContactRepository private constructor(context: Context){
    private val database : ContactDatabase = Room.databaseBuilder(
        context.applicationContext,
        ContactDatabase::class.java,
        DATABASE_NAME
    ).build()
    private val crimeDao = database.contactDao()
    private val executor = Executors.newSingleThreadExecutor()

    fun getContacts(): LiveData<List<Contact>> = crimeDao.getContacts()
    fun getContact(id: UUID): LiveData<Contact?> = crimeDao.getContact(id)
    fun getContactByPhone(phone: String): LiveData<Contact?> = crimeDao.getContactByPhone(phone)

    fun updateContact(contact: Contact) {
        executor.execute{
            crimeDao.updateContact(contact)
        }
    }

    fun addContact(contact: Contact) {
        executor.execute {
            crimeDao.addContact(contact)
        }
    }

    fun getCount() : LiveData<Int?>? {
        return crimeDao.getCount()
    }

    companion object {
        private var INSTANCE: ContactRepository? = null

        fun initialize(context: Context) {
            if (INSTANCE == null)
                INSTANCE = ContactRepository(context)
        }

        fun get() : ContactRepository {
            return INSTANCE ?:
            throw IllegalStateException("Did not initialize")
        }
    }
}