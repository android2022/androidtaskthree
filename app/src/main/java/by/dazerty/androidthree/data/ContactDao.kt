package by.dazerty.androidthree.data

import androidx.lifecycle.LiveData
import androidx.room.*
import java.util.*


@Dao
interface ContactDao {
    @Query("SELECT * FROM contact")
    fun getContacts(): LiveData<List<Contact>>

    @Query("SELECT * FROM contact WHERE id=(:id)")
    fun getContact(id: UUID): LiveData<Contact?>

    @Query("SELECT * FROM contact WHERE phone = (:phone) LIMIT 1")
    fun getContactByPhone(phone: String): LiveData<Contact?>

    @Update
    fun updateContact(contact: Contact)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addContact(contact: Contact)

    @Query("SELECT COUNT(*) FROM contact")
    fun getCount(): LiveData<Int?>?
}