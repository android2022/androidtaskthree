package by.dazerty.androidthree

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import by.dazerty.androidthree.data.Contact
import by.dazerty.androidthree.data.ContactRepository
import by.dazerty.androidthree.dialogs.ContactListDialog
import by.dazerty.androidthree.dialogs.SavedContactListDialog
import by.dazerty.androidthree.models.ContactViewModel
import by.dazerty.androidthree.models.ContactsViewModel
import by.dazerty.androidthree.providers.ContactsProvider
import com.google.android.material.snackbar.Snackbar

const val SHARED_PREF_FILE = "by.dazerty.androidthree.CONTACTS"
const val REQUEST_CODE_MAIN_ACTIVITY = 777
const val CONTACT_LIST_DIALOG = "ContactListDialog"
const val SAVED_CONTACT_LIST_DIALOG = "SavedContactListDialog"

class MainActivity : AppCompatActivity(),
        ContactListDialog.ContactListDialogListener,
        SavedContactListDialog.SavedContactListDialogListener {
    private lateinit var chooseContact : Button
    private lateinit var showContacts : Button
    private lateinit var showFromSP : Button
    private lateinit var showInNotification : Button

    private lateinit var loadedContactDataView : LinearLayout
    private lateinit var lcPhone : TextView
    private lateinit var lcName : TextView
    private lateinit var lcLastname : TextView
    private lateinit var lcEmail : TextView

    private lateinit var contactRepository : ContactRepository
    private val contactsProvider = ContactsProvider()
    private lateinit var contactViewModel : ContactViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        contactRepository = ContactRepository.get()
        contactViewModel = ViewModelProvider(this).get(ContactViewModel::class.java)

        chooseContact = findViewById(R.id.choose_contact)
        showContacts = findViewById(R.id.show_contacts)
        showFromSP = findViewById(R.id.show_from_sp)
        showInNotification = findViewById(R.id.show_in_notification)

        loadedContactDataView = findViewById(R.id.loaded_contact_data)
        lcPhone = findViewById(R.id.lc_phone)
        lcName = findViewById(R.id.lc_name)
        lcLastname = findViewById(R.id.lc_lastname)
        lcEmail = findViewById(R.id.lc_email)

        showContactDataOnView()

        chooseContact.setOnClickListener {
            if (checkPermission()) {
                val dialog = ContactListDialog()
                dialog.show(supportFragmentManager, CONTACT_LIST_DIALOG)
            } else {
                Toast.makeText(this, getString(R.string.no_perm_for_contact),
                    Toast.LENGTH_SHORT).show()
            }
        }

        showContacts.setOnClickListener {
            val dialog = SavedContactListDialog()
            dialog.show(supportFragmentManager, SAVED_CONTACT_LIST_DIALOG)
        }

        showFromSP.setOnClickListener {
            val sharedPref = getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE) ?: return@setOnClickListener
            val currentPhone : String = sharedPref.getString(getString(R.string.phone_number), null)
                ?: return@setOnClickListener

            Snackbar.make(it, getString(R.string.text_phone, currentPhone), Snackbar.LENGTH_LONG)
                .show()
        }

        showInNotification.setOnClickListener {
            val sharedPref = getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE) ?: return@setOnClickListener
            var currentPhone : String = sharedPref.getString(getString(R.string.phone_number), null)
                ?: return@setOnClickListener

            contactRepository.getContactByPhone(currentPhone).observe(
                this
            ) { contact ->
                contact?.let {
                    var builder = NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.baseline_face_black_24dp)
                        .setContentTitle(getString(R.string.saved_contact))
                        .setContentText(getString(R.string.text_name, contact.name) + " " +
                                getString(R.string.text_lastname, contact.lastname)
                        )
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)

                    with(NotificationManagerCompat.from(this)) {
                        notify(0, builder.build())
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE_MAIN_ACTIVITY) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val dialog = ContactListDialog()
                dialog.show(supportFragmentManager, CONTACT_LIST_DIALOG)
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onContactChosen(phone: String) {
        saveContactDataInDB(phone)
    }

    override fun onSavedContactChosen(contact: Contact?) {
        if (contact == null) return

        val sharedPref = getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(getString(R.string.phone_number), contact.phone)
            apply()
        }

        updateModel(contact)
    }

    private fun checkPermission() : Boolean {
        val status = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
        if (status == PackageManager.PERMISSION_GRANTED) {
            return true
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS),
                REQUEST_CODE_MAIN_ACTIVITY)
        }

        return false
    }

    private fun saveContactDataInDB(phone: String) {
        val contact = contactsProvider.getContactByPhone(this, phone) ?: return

        contactRepository.addContact(contact)
        Toast.makeText(this,getString(R.string.successful_saving), Toast.LENGTH_LONG).show()
    }

    private fun updateModel(contact: Contact) {
        contactViewModel.updateContact(contact)
        showContactDataOnView()
    }

    private fun showContactDataOnView() {
        loadedContactDataView.visibility = if (!contactViewModel.isEmptyModel()) View.VISIBLE else View.INVISIBLE

        lcPhone.text = getString(R.string.text_phone, contactViewModel.phone)
        lcName.text = getString(R.string.text_name, contactViewModel.name)
        lcLastname.text = getString(R.string.text_lastname, contactViewModel.lastname)
        lcEmail.text = getString(R.string.text_email, contactViewModel.email)
    }
}