package by.dazerty.androidthree.providers

import android.content.Context
import android.provider.ContactsContract
import by.dazerty.androidthree.data.Contact

class ContactsProvider {
     fun getContactByPhone(context: Context, phone: String) : Contact? {
        var contact : Contact? = null

        val cursor = context.contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null, ContactsContract.CommonDataKinds.Phone.NUMBER + " = ?", arrayOf(phone), null)

        if (cursor != null) {
            var name : String = ""
            var lastname : String = ""
            var email : String = ""
            contact = Contact()

            if (cursor.moveToNext()) {
                val fullName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_ALTERNATIVE) ?: 0)//display_name_alt
                if (fullName.isNotEmpty()) {
                    if (fullName.contains(",")) {
                        lastname = fullName.split(",")[0].trim()
                        name = fullName.split(",")[1].trim()
                    }
                } else {
                    name = fullName
                    lastname = ""
                }
            }

            val emailCursor = context.contentResolver.query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                arrayOf(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID) ?: 0)),
                null)
            if (emailCursor != null) {
                if (emailCursor.moveToNext()) {
                    email = emailCursor.getString(
                        emailCursor.getColumnIndex(
                            ContactsContract.CommonDataKinds.
                            Email.ADDRESS) ?: 0);
                }

                emailCursor.close()
            }

            contact.name = name
            contact.lastname = lastname
            contact.phone = phone
            contact.email = email
        }
        cursor?.close()

        return contact
    }
}