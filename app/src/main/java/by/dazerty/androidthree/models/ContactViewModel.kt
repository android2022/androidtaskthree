package by.dazerty.androidthree.models

import androidx.lifecycle.ViewModel
import by.dazerty.androidthree.data.Contact

class ContactViewModel : ViewModel() {
    var name: String = ""
    var lastname: String = ""
    var phone: String = ""
    var email: String = ""

    fun updateContact(contact: Contact) {
        name = contact.name
        lastname = contact.lastname
        phone = contact.phone
        email = contact.email
    }

    fun isEmptyModel() : Boolean {
        return name.isEmpty() && lastname.isEmpty() && phone.isEmpty() && email.isEmpty()
    }
}