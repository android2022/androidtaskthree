package by.dazerty.androidthree.models

import androidx.lifecycle.ViewModel
import by.dazerty.androidthree.data.Contact
import by.dazerty.androidthree.data.ContactRepository

class ContactsViewModel : ViewModel() {

    private val contactRepository = ContactRepository.get()
    val contactListLiveData = contactRepository.getContacts()

    fun addContact(contact: Contact) {
        contactRepository.addContact(contact)
    }

    fun getCount() : Int {
        return contactRepository.getCount()?.value ?: 0
    }
}