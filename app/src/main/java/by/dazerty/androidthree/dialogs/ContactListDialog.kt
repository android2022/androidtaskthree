package by.dazerty.androidthree.dialogs

import android.app.Dialog
import android.content.Context
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import androidx.appcompat.app.AlertDialog
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.fragment.app.DialogFragment
import androidx.loader.app.LoaderManager
import androidx.loader.content.CursorLoader
import androidx.loader.content.Loader
import by.dazerty.androidthree.R

private val FROM_COLUMNS: Array<String> = arrayOf(
        ContactsContract.CommonDataKinds.Phone.NUMBER
)
private val TO_IDS: IntArray = intArrayOf(R.id.phone_text)

class ContactListDialog: DialogFragment(),
    LoaderManager.LoaderCallbacks<Cursor> {

    interface ContactListDialogListener {
        fun onContactChosen(phone: String)
    }
    private var listener: ContactListDialogListener? = null
    private var cursorAdapter: SimpleCursorAdapter? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        LoaderManager.getInstance(this)
            .initLoader(0, null, this)

        cursorAdapter = SimpleCursorAdapter(activity,
            R.layout.contact_list_item,
            null,
            FROM_COLUMNS, TO_IDS,
            0
        )

        return activity?.let {

            val builder = AlertDialog.Builder(it)
            builder.apply {
                setTitle(getString(R.string.choose_phone))

                setAdapter(cursorAdapter
                ) { dialog, id ->
                    var cursor = cursorAdapter?.getItem(id) as Cursor
                    val phone = cursor.getString(cursor.getColumnIndex(
                        ContactsContract.CommonDataKinds.
                        Phone.NUMBER) ?: 0)
                    listener?.onContactChosen(phone)
                }
            }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as ContactListDialogListener
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<Cursor> {
        return activity?.let {
            return CursorLoader(
                it,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,null,null,null
            )
        } ?: throw IllegalStateException()
    }

    override fun onLoadFinished(loader: Loader<Cursor>, cursor: Cursor?) {
        cursorAdapter?.swapCursor(cursor)
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {
        cursorAdapter?.swapCursor(null)
    }
}