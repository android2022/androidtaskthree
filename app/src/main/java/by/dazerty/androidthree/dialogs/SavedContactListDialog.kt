package by.dazerty.androidthree.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import by.dazerty.androidthree.models.ContactsViewModel
import by.dazerty.androidthree.R
import by.dazerty.androidthree.data.Contact

class SavedContactListDialog: DialogFragment() {

    interface SavedContactListDialogListener {
        fun onSavedContactChosen(contact: Contact?)
    }

    private lateinit var phonesAdapter : PhoneAdapter
    private var listener : SavedContactListDialogListener? = null
    private lateinit var contactsList : ContactsViewModel

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        contactsList = ViewModelProvider(this@SavedContactListDialog).get(ContactsViewModel::class.java)
        phonesAdapter = PhoneAdapter()

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setTitle(getString(R.string.contacts_from_db))

                setAdapter(phonesAdapter) { dialog, id ->
                    listener?.onSavedContactChosen(phonesAdapter.getItem(id))
                }
            }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as SavedContactListDialogListener
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    inner class PhoneAdapter() : ArrayAdapter<Contact>(context!!,
        R.layout.contact_list_item,
        R.id.phone_text
    ) {
        init {
            contactsList.contactListLiveData.observe(
            this@SavedContactListDialog
            ) { contacts ->
                contacts?.let {
                    this.clear()
                    this.addAll(contacts)
                    this.notifyDataSetChanged()
                }
            }
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var view = convertView ?: LayoutInflater.from(context).inflate(R.layout.contact_list_item, parent, false)
            val contact: Contact? = getItem(position)
            val phoneText : TextView = view.findViewById(R.id.phone_text)
            phoneText.text = contact?.phone

            return view
        }
    }
}